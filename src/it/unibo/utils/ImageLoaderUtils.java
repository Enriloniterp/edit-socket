package it.unibo.utils;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * Class to load an image from res folder.
 */
public final class ImageLoaderUtils {
    public Image image;

    public ImageLoaderUtils() {
    }
    /**
     * Load an image and return it.
     * @param text the image path
     * @return the buffered image
     */
    public Image loadImage(String path) {
        try {
        	image = ImageIO.read(getClass().getResource(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }
}
