package it.unibo.view;

import javax.swing.JFileChooser;

import it.unibo.controller.ApplicationController;

public class SelectDirView {
	private String path = "";
	private String editPath = "";
	private ApplicationController ac;
	
	public SelectDirView(ApplicationController appcontr) {
		this.ac = appcontr;
		setEditPath();
		LoadBrowseDir();
	}
	
	private void LoadBrowseDir() {
		JFileChooser filechooser = new JFileChooser();
		filechooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = filechooser.showOpenDialog(null);
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			path = filechooser.getSelectedFile().getAbsolutePath();
		}
		path = path.replace("\\", "/");
		ApplicationController.path = this.path;
		ac.startLoading();
	}
	
	private void setEditPath() {
		editPath = System.getProperty("user.dir");
		editPath = editPath.replace("\\", "/");
		ApplicationController.editPath = this.editPath;
	}
}
